package com.example.simpleweather.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.example.simpleweather.R;
import com.example.simpleweather.managers.WeatherManager;
import com.example.simpleweather.model.Weather;
import com.example.simpleweather.utils.Constants;
import com.example.simpleweather.utils.ImageViewLoader;
import com.example.simpleweather.utils.net.providers.WeatherProvider;

/**
 * Created by sandstranger on 09.03.2018.
 */

public class WeatherActivity extends LocalizationActivity {
    private ViewHolder viewHolder;

    private static class ViewHolder {
        private TextView himTextView;
        private TextView cityNameTextView;
        private TextView descrTextView;
        private TextView tempTextView;
        private TextView pressureTextView;
        private TextView windSpeedTextView;
        private TextView windDegTextView;
        private ImageView image;

        private ViewHolder(Activity activity) {
            cityNameTextView = (TextView) activity.findViewById(R.id.cityText);
            descrTextView = (TextView) activity.findViewById(R.id.condDescr);
            image = (ImageView) activity.findViewById(R.id.condIcon);
            tempTextView = (TextView) activity.findViewById(R.id.temp);
            pressureTextView = (TextView) activity.findViewById(R.id.press);
            windSpeedTextView = (TextView) activity.findViewById(R.id.windSpeed);
            windDegTextView = (TextView) activity.findViewById(R.id.windDeg);
            himTextView = (TextView) activity.findViewById(R.id.hum);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        viewHolder = new ViewHolder(this);
        fillActivity();
    }

    private void fillActivity() {
        Weather weather = WeatherManager.findWeatherByCityName(getIntent().getStringExtra(Constants.INTENT_EXTRA_CITY_NAME_KEY));
        if (weather != null) {
            viewHolder.cityNameTextView.setText(weather.getCityName());
            viewHolder.descrTextView.setText(weather.getCondition() + " (" + weather.getWeatherDescription()+")");
            loadImage(WeatherProvider.getIconPath(weather.getIcon()));
            viewHolder.tempTextView.setText(String.valueOf(weather.getTemperature()));
            viewHolder.pressureTextView.setText(String.valueOf(weather.getPressure()));
            viewHolder.windDegTextView.setText(String.valueOf(weather.getSpeed()));
            viewHolder.windSpeedTextView.setText(String.valueOf(weather.getDegrees()));
            viewHolder.himTextView.setText(String.valueOf(weather.getHumidity()));
        }
    }

    private void loadImage(String url) {
        ImageViewLoader.loadImageFromUrl(url,viewHolder.image);
    }
}

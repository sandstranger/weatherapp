package com.example.simpleweather.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.example.simpleweather.R;
import com.example.simpleweather.managers.CityManager;
import com.example.simpleweather.model.City;
import com.example.simpleweather.utils.net.clients.WeatherClient;
import com.example.simpleweather.utils.net.listeners.SearchCityEventListener;
import com.example.simpleweather.view.adapters.SearchCityAdapter;
import com.example.simpleweather.view.dialogs.DialogManager;

import java.util.ArrayList;
import java.util.List;


public class SearchCityActivity extends LocalizationActivity {
    private ViewHolder viewHolder;
    private SearchCityAdapter adapter;
    private static class ViewHolder {
        private EditText searchView;
        private Button searchBtn;
        private ListView cityListView;

        public ViewHolder(Activity activity) {
            searchView = (EditText) activity.findViewById(R.id.search_city_edt_text);
            searchBtn = (Button) activity.findViewById(R.id.btn_search_city);
            cityListView = (ListView) activity.findViewById(R.id.cityList);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_city);
        adapter = new SearchCityAdapter(new ArrayList<City>(),this);
        viewHolder = new ViewHolder(this);
        viewHolder.cityListView.setAdapter(adapter);
        viewHolder.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchCity(viewHolder.searchView.getEditableText().toString());
            }
        });

        viewHolder.cityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                City city = (City) parent.getItemAtPosition(position);
                CityManager.addCity(city);
                SearchCityActivity.this.finish();
            }
        });
    }

    private void  searchCity (String citySearchPattern){
        if (citySearchPattern.isEmpty()){
            return;
        }
        viewHolder.searchBtn.setEnabled(false);
        WeatherClient cityClient = new WeatherClient(SearchCityActivity.this);
        cityClient.searchCity(citySearchPattern, new SearchCityEventListener() {
            @Override
            public void onCitiesListRetrieved(List<City> cityList) {
                if (SearchCityActivity.this==null){
                    return;
                }
                viewHolder.searchBtn.setEnabled(true);
                rebuildAdapter(cityList);
            }

            @Override
            public void onConnectionFailed() {
                viewHolder.searchBtn.setEnabled(true);
                DialogManager.showNoInternetDialog(SearchCityActivity.this);
            }
        });
    }

    private void rebuildAdapter (List <City> cityList){
        adapter.clear();
        adapter.addAll(cityList);
        adapter.notifyDataSetChanged();
    }
}

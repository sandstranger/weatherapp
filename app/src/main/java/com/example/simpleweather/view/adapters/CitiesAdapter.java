package com.example.simpleweather.view.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.simpleweather.R;
import com.example.simpleweather.managers.CityManager;
import com.example.simpleweather.managers.WeatherManager;
import com.example.simpleweather.model.City;
import com.example.simpleweather.model.Weather;
import com.example.simpleweather.utils.Constants;
import com.example.simpleweather.utils.net.clients.WeatherClient;
import com.example.simpleweather.utils.net.listeners.WeatherEventListener;
import com.example.simpleweather.view.activity.WeatherActivity;
import com.example.simpleweather.view.dialogs.DialogListener;
import com.example.simpleweather.view.dialogs.DialogManager;

import java.util.ArrayList;

public class CitiesAdapter extends ArrayAdapter<City> {
    private Activity activity;

    private static class ViewHolder {
        TextView cityName;
        Button btnDeleteCity;
        Button btnGetWeather;
    }

    public CitiesAdapter(ArrayList<City> data, Activity activity) {
        super(activity, R.layout.city_item, data);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final City cityModel = getItem(position);
        final ViewHolder viewHolder;
        View resultView;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.city_item, parent, false);

            viewHolder.cityName = (TextView) convertView.findViewById(R.id.txt_city);
            viewHolder.btnDeleteCity = (Button) convertView.findViewById(R.id.btn_delete_city);
            viewHolder.btnGetWeather = (Button) convertView.findViewById(R.id.btn_show_weather);

            convertView.setTag(viewHolder);
            resultView = convertView;
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            resultView = convertView;
        }
        viewHolder.cityName.setText(cityModel.getCityName());
        viewHolder.btnDeleteCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog(cityModel);
            }
        });
        viewHolder.btnGetWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findWeatherForCity(cityModel.getCityName());
            }
        });

        return resultView;
    }

    private void showDeleteDialog(final City cityToRemove) {
        String dialogMessage = activity.getString(R.string.delete_city);
        DialogManager.showDialog(activity, String.format(dialogMessage, cityToRemove.getCityName()), new DialogListener() {
            @Override
            public void onPositiveBtnClicked() {
                if (CitiesAdapter.this == null) {
                    return;
                }
                WeatherManager.deleteWeather(cityToRemove.getCityName());
                CityManager.deleteCity(cityToRemove);
                CitiesAdapter.this.remove(cityToRemove);
                CitiesAdapter.this.notifyDataSetChanged();
            }

            @Override
            public void onCloseBtnClicked() {
            }
        });
    }

    private void findWeatherForCity(final String cityName) {
        WeatherClient client = new WeatherClient(activity);
        client.getWeatherForCity(cityName, new WeatherEventListener() {
            @Override
            public void onWeatherRetrieved(Weather weather) {
                weather.setCityName(cityName);
                WeatherManager.addWeather(weather);
                startWeatherActivity(cityName);
            }

            @Override
            public void onConnectionFailed() {
                if (WeatherManager.weatherExists(cityName)){
                    startWeatherActivity(cityName);
                }
                else {
                    DialogManager.showNoInternetDialog(activity);
                }
            }
        });
    }

    private void startWeatherActivity(String cityName) {
        Intent intent = new Intent(activity, WeatherActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_CITY_NAME_KEY, cityName);
        activity.startActivity(intent);
    }
}

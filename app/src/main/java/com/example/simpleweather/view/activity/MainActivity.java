package com.example.simpleweather.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.example.simpleweather.R;
import com.example.simpleweather.managers.CityManager;
import com.example.simpleweather.model.City;
import com.example.simpleweather.view.adapters.CitiesAdapter;

import java.util.ArrayList;
import java.util.Comparator;

public class MainActivity extends LocalizationActivity {
    private CitiesAdapter citiesAdapter;

    private static final Comparator<City> comparator = new Comparator<City>() {
        @Override
        public int compare(City city1, City city2) {
            return city1.getCityName().compareTo(city2.getCityName());
        }
    };

    private static class ViewHolder {
        Toolbar toolbar;
        FloatingActionButton fab;
        ListView listView;

        private ViewHolder(Activity activity) {
            toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
            listView = (ListView) activity.findViewById(R.id.citiesList);
            fab = (FloatingActionButton) activity.findViewById(R.id.btn_add_city);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewHolder viewHolder = new ViewHolder(MainActivity.this);
        citiesAdapter = new CitiesAdapter(new ArrayList<City>(), MainActivity.this);
        viewHolder.listView.setAdapter(citiesAdapter);
        fillList();
        viewHolder.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearchActivity();
            }
        });
        setSupportActionBar(viewHolder.toolbar);

    }


    @Override
    public void onResume() {
        super.onResume();
        //TODO Этот костыль нужно переделать
        fillList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startSettingsActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fillList() {
        if (citiesAdapter == null) {
            return;
        }
        citiesAdapter.clear();
        citiesAdapter.addAll(CityManager.getCities());
        citiesAdapter.sort(comparator);
        citiesAdapter.notifyDataSetChanged();
    }

    private void startSearchActivity() {
        startActivity(new Intent(this, SearchCityActivity.class));
    }

    private void startSettingsActivity() {
        startActivity(new Intent(this, SettingsActivity.class));
    }
}

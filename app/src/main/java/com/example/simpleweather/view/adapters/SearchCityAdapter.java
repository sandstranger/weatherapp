package com.example.simpleweather.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.simpleweather.R;
import com.example.simpleweather.model.City;
import java.util.ArrayList;


public class SearchCityAdapter extends ArrayAdapter<City> {
    private Context mContext;

    private static class ViewHolder {
        private TextView cityNameTextView;
        private TextView countryCodeTextView;
    }

    public SearchCityAdapter(ArrayList<City> data, Context context) {
        super(context, R.layout.search_city_item, data);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final City cityModel = getItem(position);
        final ViewHolder viewHolder;
        View resultView;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate( R.layout.search_city_item, parent, false);

            viewHolder.cityNameTextView = (TextView) convertView.findViewById(R.id.search_city_name);
            viewHolder.countryCodeTextView = (TextView) convertView.findViewById(R.id.search_city_country_code);

            convertView.setTag(viewHolder);
            resultView = convertView;
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            resultView = convertView;
        }
        viewHolder.cityNameTextView.setText(cityModel.getCityName());
        viewHolder.countryCodeTextView.setText(cityModel.getCountryCode());
        return resultView;
    }
}

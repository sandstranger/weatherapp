package com.example.simpleweather.view.dialogs;

/**
 * Created by sandstranger on 08.03.2018.
 */

public interface DialogListener {
    void onPositiveBtnClicked();
    void onCloseBtnClicked ();
}

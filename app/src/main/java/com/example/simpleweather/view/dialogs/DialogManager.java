package com.example.simpleweather.view.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.simpleweather.R;

public class DialogManager {

    public static void showNoInternetDialog(Context context) {
        showOkDialog(context, context.getString(R.string.no_internet_message));
    }

    public static void showOkDialog(@NonNull Context context, String message) {
        new MaterialDialog.Builder(context)
                .positiveText(context.getString(R.string.dialog_ok_text))
                .content(message)
                .show();
    }

    public static void showDialog(@NonNull Context context, String message, @Nullable final DialogListener dialogListener) {
        new MaterialDialog.Builder(context)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (dialogListener != null) {
                            dialogListener.onPositiveBtnClicked();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (dialogListener != null) {
                            dialogListener.onCloseBtnClicked();
                        }
                    }
                })
                .positiveText(context.getString(R.string.dialog_ok_btn_text))
                .negativeText(context.getString(R.string.dialog_close_btn_text))
                .content(message)
                .show();
    }
}

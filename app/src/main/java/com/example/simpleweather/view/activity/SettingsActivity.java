package com.example.simpleweather.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.example.simpleweather.R;
import com.example.simpleweather.managers.CityManager;
import com.example.simpleweather.managers.DatabaseManager;
import com.example.simpleweather.utils.Constants;

public class SettingsActivity extends LocalizationActivity {

    private ViewHolder viewHolder;

    private static class ViewHolder{
        private TextView cityCountTextView;
        private Button btnDeleteAll;
        private Button btnChangeLanguage;

        private ViewHolder (Activity activity){
            cityCountTextView = (TextView) activity.findViewById(R.id.city_count);
            btnDeleteAll = (Button) activity.findViewById(R.id.btn_clear_database);
            btnChangeLanguage = (Button) activity.findViewById(R.id.btn_change_language);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_view);
        viewHolder = new ViewHolder(this);
        fillActivity();
    }

    private void  fillActivity (){
        updateCityCount();
        viewHolder.btnDeleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseManager.clearDatabase();
                updateCityCount();
            }
        });

        viewHolder.btnChangeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeLanguageDialog();
            }
        });
    }

    private void updateCityCount (){
        viewHolder.cityCountTextView.setText(String.valueOf(CityManager.getCities().size()));
    }

    //Нам не нужно использовать ListPreferences , плагин локализации все знает и без нас и сохраняет в SharedPreferences язык автоматически
    private void showChangeLanguageDialog (){
        new MaterialDialog.Builder(this)
                .title(R.string.btn_change_lang_text)
                .items(Constants.SUPPORTED_LOCALES)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        setLanguage(text.toString());
                    }
                })
                .show();
    }
}

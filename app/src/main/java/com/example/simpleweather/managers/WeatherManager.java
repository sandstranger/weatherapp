package com.example.simpleweather.managers;

import android.support.annotation.Nullable;

import com.example.simpleweather.model.Weather;

import java.util.ArrayList;
import java.util.List;

public class WeatherManager {

    public static List<Weather> getWeatherList() {
        List <Weather> weatherList = Weather.listAll(Weather.class);
        return weatherList != null ? weatherList : new ArrayList<Weather>();
    }

    public static void addWeather(Weather weather) {
        Weather oldWeather = findWeatherByCityName(weather.getCityName());
        if (oldWeather != null) {
            oldWeather.update(weather);
        } else {
            weather.save();
        }
    }

    public static void deleteWeather(String cityName) {
        Weather weather = findWeatherByCityName(cityName);
            if (weather != null){
                weather.delete();
        }
    }

    @Nullable
    public static Weather findWeatherByCityName(String cityName) {
        List<Weather> weatherList = getWeatherList();
        for (Weather weather : weatherList) {
            if (weather.getCityName().equals(cityName))
                return weather;
        }
        return null;
    }

    public static boolean weatherExists(String cityName) {
        return findWeatherByCityName(cityName) != null;
    }
}

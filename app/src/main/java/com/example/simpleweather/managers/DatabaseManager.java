package com.example.simpleweather.managers;

import com.example.simpleweather.model.City;
import com.example.simpleweather.model.Weather;


public class DatabaseManager {

    public static void clearDatabase() {
        City.deleteAll(City.class);
        Weather.deleteAll(Weather.class);
    }
}

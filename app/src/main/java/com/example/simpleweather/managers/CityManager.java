package com.example.simpleweather.managers;

import android.support.annotation.Nullable;

import com.example.simpleweather.model.City;

import java.util.ArrayList;
import java.util.List;

public class CityManager {

    public static List<City> getCities() {
        List <City> cityList = City.listAll(City.class);
        return cityList != null ? cityList : new ArrayList<City>();
    }

    public static void addCity(City city) {
        if (cityExists(city.getCityName())) {
            return;
        }
        city.save();
    }

    public static void deleteCity(City city) {
        if (city == null){
            return;
        }
        city.delete();
    }

    @Nullable
    private static City findCityByName(String cityName) {
        List<City> cities = getCities();
        for (City city : cities) {
            if (city.getCityName().equals(cityName))
                return city;
        }
        return null;
    }

    public static boolean cityExists(String cityName) {
        return findCityByName(cityName) != null;
    }
}

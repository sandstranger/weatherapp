package com.example.simpleweather.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class City extends SugarRecord<City> {

    @SerializedName("c")
    private String countryCode;

    @SerializedName("name")
    private String cityName;

    public City (){

    }

    public City (String cityName){
        this.cityName = cityName;
    }

    public String getCityName (){
        return cityName;
    }

    public String getCountryCode (){
        return countryCode;
    }
}

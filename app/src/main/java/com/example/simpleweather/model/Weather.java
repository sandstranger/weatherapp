package com.example.simpleweather.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Weather extends SugarRecord {
    @SerializedName("speed")
    private float speed;
    @SerializedName("deg")
    private float degrees;
    @SerializedName("temperature")
    private float temperature;
    @SerializedName("humidity")
    private int humidity;
    @SerializedName("pressure")
    private int pressure;
    private String cityName;
    @SerializedName("main")
    private String condition;
    @SerializedName("description")
    private String description;
    @SerializedName("icon")
    private String icon;

    public Weather() {

    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition (String condition){
        this.condition = condition;
    }

    public void  setDescription (String description){
        this.description =description;
    }

    public  void setIcon (String icon){
        this.icon = icon;
    }

    public String getWeatherDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public float getSpeed (){
        return speed;
    }

    public void setSpeed (float speed){
        this.speed = speed;
    }

    public float getDegrees (){
        return degrees;
    }

    public void  setDegrees (float degrees){
        this.degrees = degrees;
    }

    public float getTemperature (){
        return temperature;
    }

    public void setTemperature (float temperature){
        this.temperature = temperature;
    }

    public int getHumidity (){
        return humidity;
    }

    public void setHumidity (int humidity){
        this.humidity = humidity;
    }

    public int getPressure (){
        return  pressure;
    }

    public void setPressure (int pressure){
        this.pressure = pressure;
    }


    public void update (Weather weather){
        speed = weather.speed;
        degrees = weather.degrees;
        temperature = weather.temperature;
        humidity = weather.humidity;
        pressure = weather.pressure;
        condition = weather.condition;
        description = weather.description;
        icon = weather.icon;
        save();
    }
}

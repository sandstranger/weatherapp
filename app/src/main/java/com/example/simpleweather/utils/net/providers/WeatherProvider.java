package com.example.simpleweather.utils.net.providers;

import android.app.Activity;

import com.example.simpleweather.R;
import com.example.simpleweather.model.City;
import com.example.simpleweather.model.Weather;
import com.example.simpleweather.utils.LogcatPrinter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeatherProvider {
    private Activity activity;
    private static final String SEARCH_CITY_URL = "http://autocomplete.wunderground.com/aq?query=%s";
    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s";
    private static final String ICON_PATH = "http://openweathermap.org/img/w/";

    public WeatherProvider(Activity activity) {
        this.activity = activity;
    }

    public String getSearchCityQueryUrl(String searchPattern) {
        return String.format(SEARCH_CITY_URL, searchPattern);
    }

    public String getWeatherQueryUrl(String cityName) {
        return String.format(WEATHER_URL, cityName, getApiKey());
    }

    public Weather getWeather(String responce) {
        try {
            LogcatPrinter.printMessageToLogcat(responce);
            com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
            JsonElement element = jsonParser.parse(responce);
            JsonObject weatherObj = element.getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject();
            Weather weather = new Weather();
            weather.setCondition(weatherObj.getAsJsonPrimitive("main").getAsString());
            weather.setDescription(weatherObj.getAsJsonPrimitive("description").getAsString());
            weather.setIcon(weatherObj.getAsJsonPrimitive("icon").getAsString());
            JsonObject weatherMain = element.getAsJsonObject().getAsJsonObject("main");
            weather.setTemperature(weatherMain.getAsJsonPrimitive("temp").getAsFloat());
            weather.setHumidity(weatherMain.getAsJsonPrimitive("humidity").getAsInt());
            weather.setPressure(weatherMain.getAsJsonPrimitive("pressure").getAsInt());
            JsonObject windObj = element.getAsJsonObject().getAsJsonObject("wind");
            weather.setSpeed(windObj.getAsJsonPrimitive("speed").getAsFloat());
            weather.setDegrees(windObj.getAsJsonPrimitive("deg").getAsFloat());
            return weather;
        } catch (Exception e) {
            LogcatPrinter.printExceptionToLogcat(e);
        }
        return new Weather();
    }

    public List<City> getCityList(String responce) {
        List<City> cityList = new ArrayList<City>();
        try {
            LogcatPrinter.printMessageToLogcat(responce);
            com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
            JsonElement element = jsonParser.parse(responce);
            JsonArray obj = element.getAsJsonObject().getAsJsonArray("RESULTS");
            Gson gson = new Gson();
            cityList = new ArrayList<City>(Arrays.asList(gson.fromJson(obj, City[].class)));
        } catch (Exception e) {
            LogcatPrinter.printExceptionToLogcat(e);
        }
        return cityList;
    }

    public static String getIconPath(String weatherIconName) {
        return ICON_PATH + weatherIconName + ".png";
    }

    private String getApiKey() {
        return activity.getString(R.string.api_key);
    }
}

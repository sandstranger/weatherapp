package com.example.simpleweather.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sandstranger on 05.03.2018.
 */

public class Constants {
    public static final String INTENT_EXTRA_CITY_NAME_KEY = "INTENT_EXTRA_CITY";
    public static final List<String> SUPPORTED_LOCALES = Arrays.asList("en", "ru");
}

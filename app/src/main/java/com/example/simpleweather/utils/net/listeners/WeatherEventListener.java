package com.example.simpleweather.utils.net.listeners;

import com.example.simpleweather.model.Weather;

public interface WeatherEventListener {
    void onWeatherRetrieved(Weather weather);

    void onConnectionFailed();
}

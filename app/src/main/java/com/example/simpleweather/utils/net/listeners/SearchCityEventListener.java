package com.example.simpleweather.utils.net.listeners;

import com.example.simpleweather.model.City;

import java.util.List;

public interface SearchCityEventListener {

    void onCitiesListRetrieved(List<City> cityList);

    void onConnectionFailed();

}

package com.example.simpleweather.utils.net.clients;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.example.simpleweather.utils.net.listeners.SearchCityEventListener;
import com.example.simpleweather.utils.net.listeners.WeatherEventListener;
import com.example.simpleweather.utils.net.providers.WeatherProvider;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class WeatherClient {
    private static final int CONNECTION_TIMEOUT = 1;
    private WeatherProvider weatherProvider;
    private Activity activity;

    public WeatherClient(Activity activity) {
        this.activity = activity;
        weatherProvider = new WeatherProvider(activity);
    }

    public void searchCity(String searchPattern, SearchCityEventListener eventListener) {
        String url = weatherProvider.getSearchCityQueryUrl(searchPattern);
        executeSeachCityRequest(url, eventListener);
    }

    public void getWeatherForCity(String cityName, WeatherEventListener eventListener) {
        executeSearchWeatherRequest(weatherProvider.getWeatherQueryUrl(cityName), eventListener);
    }

    private void executeSearchWeatherRequest(@NonNull String url, final WeatherEventListener eventListener) {
        OkHttpClient client = buildHttpClient();
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (eventListener != null) {
                            eventListener.onConnectionFailed();
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responceString = response.body().string();
                if (eventListener != null && !responceString.isEmpty()) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            eventListener.onWeatherRetrieved(weatherProvider.getWeather(responceString));
                        }
                    });
                }
            }
        });
    }

    private void executeSeachCityRequest(@NonNull String url, final SearchCityEventListener eventListener) {
        OkHttpClient client = buildHttpClient();
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (eventListener != null) {
                            eventListener.onConnectionFailed();
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responceString = response.body().string();
                if (eventListener != null && !responceString.isEmpty()) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            eventListener.onCitiesListRetrieved(weatherProvider.getCityList(responceString));
                        }
                    });
                }
            }
        });
    }

    private OkHttpClient buildHttpClient(){
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }
}
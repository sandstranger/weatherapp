package com.example.simpleweather.utils;

import android.util.Log;

import com.example.simpleweather.BuildConfig;

/**
 * Created by sandstranger on 08.03.2018.
 */

public class LogcatPrinter {
    private static final String EXCEPTIOIN_TAG = "Exception";
    private static final String APP_TAG = "WeatherApp";

    public static void printMessageToLogcat (String message){
        if (BuildConfig.DEBUG){
            Log.d(APP_TAG,message);
        }
    }

    public static void printExceptionToLogcat(Exception e){
        if (BuildConfig.DEBUG){
            Log.e(APP_TAG,EXCEPTIOIN_TAG,e);
        }
    }
}


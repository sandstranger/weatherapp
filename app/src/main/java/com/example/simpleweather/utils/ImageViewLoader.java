package com.example.simpleweather.utils;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;


public class ImageViewLoader {

    public static void loadImageFromUrl(String url, ImageView targetImageView) {
        Picasso picasso = Picasso.get();
        picasso.load(url).into(targetImageView);
    }
}
